<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;
use Auth;
use App\Http\Requests\Api\RegisterRequest;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $loginField = request('email');
        $credentials = null;

        if(!request('password')){
            return response()->json(['code' => 422, 'message' => 'Password tidak boleh kosong'], 422);
        }

        if ($loginField !== null) {
            $loginType = filter_var($loginField, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
    
            request()->merge([ $loginType => $loginField ]);
    
            $credentials = request([ $loginType, 'password' ]);
        } else {
            return response()->json(['code' => 422, 'message' => 'email tidak boleh kosong'], 422);
        }

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['code' => 400, 'message' => 'Alamat Email Atau Password Salah'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['code' => 500, 'message' => 'Tidak Dapat Membuat Token'], 500);
        }
        return response()->json(['code' => 200, 'message' => 'Berhasil Login', 'data' => ['token' => $token]], 200);
    }
    public function profile(){
        return response()->json(['code' => 200, 'message' => 'Berhasil mengambil data profile', 'data' => ['profile' => auth()->user()]], 200);
    }
    public function register(RegisterRequest $request){
        $create_user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password'))
        ]);
        return response()->json(['code' => 200, 'message' => 'Berhasil Mendaftar'],200);
    }
    public function logout(){
        try {
            $token = JWTAuth::getToken();
            if ($token) {
                JWTAuth::invalidate($token);
            }
        } catch (JWTException $e) {
            return response()->json(['code' => 500, 'message' => $e->getMessage()], 500);
        }
        return response()->json(['code' => 200, 'message' => 'Berhasil Keluar !'], 200);
    }
}