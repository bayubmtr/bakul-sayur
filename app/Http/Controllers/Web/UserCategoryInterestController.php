<?php

namespace App\Http\Controllers;

use App\Models\Users\UserCategoryInterest;
use Illuminate\Http\Request;

class UserCategoryInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Users\UserCategoryInterest  $userCategoryInterest
     * @return \Illuminate\Http\Response
     */
    public function show(UserCategoryInterest $userCategoryInterest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Users\UserCategoryInterest  $userCategoryInterest
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCategoryInterest $userCategoryInterest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Users\UserCategoryInterest  $userCategoryInterest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserCategoryInterest $userCategoryInterest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users\UserCategoryInterest  $userCategoryInterest
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCategoryInterest $userCategoryInterest)
    {
        //
    }
}
