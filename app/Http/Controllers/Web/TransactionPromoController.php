<?php

namespace App\Http\Controllers;

use App\Models\Transactions\TransactionPromo;
use Illuminate\Http\Request;

class TransactionPromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transactions\TransactionPromo  $transactionPromo
     * @return \Illuminate\Http\Response
     */
    public function show(TransactionPromo $transactionPromo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transactions\TransactionPromo  $transactionPromo
     * @return \Illuminate\Http\Response
     */
    public function edit(TransactionPromo $transactionPromo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transactions\TransactionPromo  $transactionPromo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransactionPromo $transactionPromo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transactions\TransactionPromo  $transactionPromo
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransactionPromo $transactionPromo)
    {
        //
    }
}
