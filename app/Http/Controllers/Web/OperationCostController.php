<?php

namespace App\Http\Controllers;

use App\Models\Webs\OperationCost;
use Illuminate\Http\Request;

class OperationCostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Webs\OperationCost  $operationCost
     * @return \Illuminate\Http\Response
     */
    public function show(OperationCost $operationCost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Webs\OperationCost  $operationCost
     * @return \Illuminate\Http\Response
     */
    public function edit(OperationCost $operationCost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Webs\OperationCost  $operationCost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OperationCost $operationCost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Webs\OperationCost  $operationCost
     * @return \Illuminate\Http\Response
     */
    public function destroy(OperationCost $operationCost)
    {
        //
    }
}
