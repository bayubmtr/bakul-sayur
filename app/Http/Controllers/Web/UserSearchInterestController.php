<?php

namespace App\Http\Controllers;

use App\Models\Users\UserSearchInterest;
use Illuminate\Http\Request;

class UserSearchInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Users\UserSearchInterest  $userSearchInterest
     * @return \Illuminate\Http\Response
     */
    public function show(UserSearchInterest $userSearchInterest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Users\UserSearchInterest  $userSearchInterest
     * @return \Illuminate\Http\Response
     */
    public function edit(UserSearchInterest $userSearchInterest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Users\UserSearchInterest  $userSearchInterest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserSearchInterest $userSearchInterest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users\UserSearchInterest  $userSearchInterest
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserSearchInterest $userSearchInterest)
    {
        //
    }
}
