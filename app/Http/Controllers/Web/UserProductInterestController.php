<?php

namespace App\Http\Controllers;

use App\Models\Users\UserProductInterest;
use Illuminate\Http\Request;

class UserProductInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Users\UserProductInterest  $userProductInterest
     * @return \Illuminate\Http\Response
     */
    public function show(UserProductInterest $userProductInterest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Users\UserProductInterest  $userProductInterest
     * @return \Illuminate\Http\Response
     */
    public function edit(UserProductInterest $userProductInterest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Users\UserProductInterest  $userProductInterest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserProductInterest $userProductInterest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users\UserProductInterest  $userProductInterest
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserProductInterest $userProductInterest)
    {
        //
    }
}
