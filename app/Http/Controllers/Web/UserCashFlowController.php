<?php

namespace App\Http\Controllers;

use App\Models\Users\UserCashFlow;
use Illuminate\Http\Request;

class UserCashFlowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Users\UserCashFlow  $userCashFlow
     * @return \Illuminate\Http\Response
     */
    public function show(UserCashFlow $userCashFlow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Users\UserCashFlow  $userCashFlow
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCashFlow $userCashFlow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Users\UserCashFlow  $userCashFlow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserCashFlow $userCashFlow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users\UserCashFlow  $userCashFlow
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCashFlow $userCashFlow)
    {
        //
    }
}
