<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionPromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_promos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('transaction_id')->unsigned();
            $table->bigInteger('promo_id')->unsigned()->nullable();
            $table->enum('type', ['discount', 'cashback', 'gratis ongkir']);
            $table->integer('obtain')->nullable();

            $table->foreign('transaction_id')->references('id')->on('transactions')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('promo_id')->references('id')->on('promos')
                  ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_promos');
    }
}
