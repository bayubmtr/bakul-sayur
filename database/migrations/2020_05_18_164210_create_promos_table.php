<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->id();
            $table->string('code', 50);
            $table->enum('type', ['discount', 'cashback', 'gratis ongkir']);
            $table->enum('value_type', ['nominal', 'percent']);
            $table->integer('value');
            $table->integer('max_obtain')->nullable();
            $table->integer('min_order_price')->nullable();
            $table->integer('min_order_unit')->nullable();
            $table->integer('max_use')->nullable();
            $table->string('banner_small');
            $table->string('banner_high');
            $table->timestamp('start_at', 0)->nullable();
            $table->timestamp('expired_at', 0)->nullable();
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}
